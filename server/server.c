#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <pthread.h>

#define PORT "4200"
#define BUFFER_SIZE 10004

fd_set fileDescriptorList;    // fileDescriptorList file descriptor list
fd_set temporaryFileDescriptor;  // temp file descriptor list for select()
int allFileDescriptors;        // maximum file descriptor number
int listener;     // listening socket descriptor
int newFileDescriptor;        // newly accept()ed socket descriptor
struct sockaddr_storage clientAddress; // client address
socklen_t addressLength;
unsigned char clientMessage[256];    // cientMessagefer for client data
int bytesRecieved;
char remoteIP[INET6_ADDRSTRLEN];
int reusable = 1;        // for setsockopt() SO_REUSEADDR, below
int rv;
struct addrinfo hints, *addressInfo, *p;

int32_t mapHeight;
int32_t mapWidth;

pthread_t acceptPlayerThread;
pthread_t sendMapThread;
pthread_t playerMoveThread;
pthread_t sendPlayersLocationThread;
pthread_t updatePlayersLocationThread;

unsigned char mapObject[BUFFER_SIZE];
unsigned char map[100][100];

typedef struct Player {
    int32_t playerId;
    unsigned char playerName[20];
    int32_t playerPositionX;
    int32_t playerPositionY;
    int32_t playerState;
    int32_t playerType;
    int32_t direction;
    int32_t score;
} Player_t;

Player_t players[8];

void *getAddress(struct sockaddr *sa);
int32_t batoi(const unsigned char bytes[4]);
void itoba(int32_t integer, unsigned char buffer[4]);
void startGame(int socket);
void clearPlayerData(int id);
void *acceptClientFunction(void *arg);
void *sendMapFunction(void *arg);
void *playerMoveFunction(void *arg);
void *sendPlayersLocationFunction(void *arg);
void *updatePlayersLocationFunction(void *arg);

int main(void) {
    //Load map from file
    FILE *mapFile;
    mapFile = fopen("maps/map1.txt", "r");

    int counter = 4;
    fscanf (mapFile,"%d %d", &mapHeight, &mapWidth);
    char c = getc(mapFile);

    while (c != EOF) {
        if (c != '\n') {
            mapObject[counter] = c;
            counter++;
        }

        c = getc(mapFile);
    }

    fclose(mapFile);

    counter = 4;
    for (int i = 0; i < mapHeight; ++i) {
       for (int j = 0; j < mapWidth; ++j) {
           map[i][j] = mapObject[counter++];
        }
    }

    FD_ZERO(&fileDescriptorList);    // clear the fileDescriptorList and temp sets
    FD_ZERO(&temporaryFileDescriptor);

    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(NULL, PORT, &hints, &addressInfo)) != 0) {
        fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
        exit(1);
    }
    
    for(p = addressInfo; p != NULL; p = p->ai_next) {

        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) { 
            continue;
        }
        
        // lose the pesky "address already in use" error message
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &reusable, sizeof(int));

        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
            close(listener);
            continue;
        }

        break;
    }

    // if we got here, it means we didn't get bound
    if (p == NULL) {
        fprintf(stderr, "selectserver: failed to bind\n");
        exit(2);
    }

    freeaddrinfo(addressInfo); // all done with this

    // listen
    if (listen(listener, 10) == -1) {
        perror("listen");
        exit(3);
    }

    // add the listener to the fileDescriptorList set
    FD_SET(listener, &fileDescriptorList);

    // keep track of the biggest file descriptor
    allFileDescriptors = listener; // so far, it's this one
    pthread_create(&updatePlayersLocationThread, NULL, updatePlayersLocationFunction, NULL);
    // main loop
    while(1) {
        temporaryFileDescriptor = fileDescriptorList; // copy it
        if (select(allFileDescriptors+1, &temporaryFileDescriptor, NULL, NULL, NULL) == -1) {
            perror("select");
            exit(4);
        }

        // run through the existing connections looking for data to read
        for(int playerConnection = 0; playerConnection <= allFileDescriptors; playerConnection++) {
            if (FD_ISSET(playerConnection, &temporaryFileDescriptor)) { // we got one!!
                if (playerConnection == listener) {
                    // handle new connections

                    addressLength = sizeof clientAddress;
                    int socket = accept(listener, (struct sockaddr *)&clientAddress, &addressLength);

                    pthread_create(&acceptPlayerThread, NULL, acceptClientFunction, &socket);
                    pthread_create(&sendPlayersLocationThread, NULL, sendPlayersLocationFunction, &socket);
                    pthread_create(&sendMapThread, NULL, sendMapFunction, &socket);
                    pthread_create(&playerMoveThread, NULL, playerMoveFunction, &playerConnection);
                } else {
                    // handle data from a client
                    if ((bytesRecieved = read(playerConnection, clientMessage, sizeof(clientMessage))) <= 0) {
                        // got error or connection closed by client
                        if (bytesRecieved == 0) {
                            // connection closed
                            printf("Player %d hung up\n", playerConnection);
                        } else {
                            printf("Player %d disconnected\n", playerConnection);
                            perror("recv");
                        }
                        pthread_join(playerMoveThread, NULL);
                        pthread_join(sendPlayersLocationThread, NULL);
                        pthread_join(sendMapThread, NULL);

                        clearPlayerData(playerConnection);
                        close(playerConnection); // bye!
                        FD_CLR(playerConnection, &fileDescriptorList); // remove from fileDescriptorList set
                    }
                }
            }
        }
    }
    return 0;
}

// get sockaddr, IPv4 or IPv6:
void *getAddress(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int32_t batoi(const unsigned char bytes[4]) {
    return (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
}

void itoba (int32_t integer, unsigned char buffer[4]) {
    buffer[0] = (integer >> 24) & 0xFF;
    buffer[1] = (integer >> 16) & 0xFF;
    buffer[2] = (integer >> 8) & 0xFF;
    buffer[3] = integer & 0xFF;
}

void startGame(int socket) {
    int sentBytes;

    unsigned char startGame[20];
    int32_t packetType = 2;
    int32_t playerPositionX = 1;
    int32_t playerPositionY = 1;

    itoba(packetType, startGame);
    itoba(mapWidth, &startGame[4]);
    itoba(mapHeight, &startGame[8]);
    itoba(playerPositionX, &startGame[12]);
    itoba(playerPositionY, &startGame[16]);

    sentBytes = write(socket, startGame, 20);

    if (sentBytes < 0) {
        perror("Error startGame");
    }

}

void clearPlayerData(int id) {
    players[id].playerId = 0;
    bzero(players[id].playerName, 20);
    players[id].playerPositionX = 0;
    players[id].playerPositionY = 0;
}

void *acceptClientFunction(void *arg) {
    unsigned char clientMessage[24];
    int socket = *((int*) arg);

    if (socket == -1) {
        perror("Error acceptClientFunction 0");
    } else {
        FD_SET(socket, &fileDescriptorList); // add to fileDescriptorList set
        if (socket > allFileDescriptors) {    // keep track of the max
            allFileDescriptors = socket;
        }
    }


    int recieve = read(socket, clientMessage, 24);
    if (recieve < 0) {
        perror("Error acceptClientFunction 1");
        exit(1);
    }

    sleep(1);

    int32_t packetType = batoi(clientMessage);
    printf("Player %d name %s connected from %s\n",socket, &clientMessage[4], inet_ntop(clientAddress.ss_family, getAddress((struct sockaddr*)&clientAddress), remoteIP, INET6_ADDRSTRLEN));

    int32_t playerPosition = socket - 4;
    int32_t playerId = playerPosition + 1;


    players[playerPosition].playerId = playerId;
    players[playerPosition].playerState = 0;
    players[playerPosition].playerType = 0;
    players[playerPosition].playerPositionX = 1;
    players[playerPosition].playerPositionY = 1;
    players[playerPosition].direction = 5;

    itoba(1, clientMessage);
    itoba(playerId, &clientMessage[4]);

    int sent = write(socket, clientMessage, 8);
    if (sent < 0) {
        perror("Error acceptClientFunction 2");
        exit(1);
    }

    sleep(1);
    startGame(socket);
}

void *sendMapFunction(void *arg) {
    pthread_join(acceptPlayerThread,NULL);
    int socket = *((int*) arg);
    itoba(4, mapObject);
    int sent;
    while(1) {
        sent = write(socket, mapObject, 10004);
        if (sent < 0) {
            perror("Error sendmapFunction");
            break;
        }
        sleep(1);
    }
    pthread_join(sendPlayersLocationThread, NULL);
}

void *playerMoveFunction(void *arg) {
    unsigned char clientMessage[12];
    int socket = *((int*) arg);
    while (1) {
        int recieve = read(socket, clientMessage, 12);
        if (recieve < 0) {
            perror("Error playerMoveFunction!");
            break;
        }

        int32_t packetType = batoi(clientMessage);
        int playerPosition;
        int positionX;
        int positionY;
        int playerPositionX;
        int playerPositionY;
        if (packetType == 7) {
            int playerId = batoi(&clientMessage[4]);
            int32_t playerMove = batoi(&clientMessage[8]);
            playerPosition = playerId - 1;

            int x = players[playerPosition].playerPositionX;
            int y = players[playerPosition].playerPositionY;

            switch(playerMove) {
                case 0  :
                    players[playerPosition].direction = 0;
                    break;
                    
                case 1  :
                    players[playerPosition].direction = 1;
                    break;

                case 2  :
                    players[playerPosition].direction = 2;
                    break;
                
                case 3  :
                    players[playerPosition].direction = 3;
                    break;
              
                default : 
                    break;
            }
        }
    }
    exit(1);
}

void *sendPlayersLocationFunction(void *arg) {
        unsigned char clientMessage[168];
        int socket = *((int*) arg);

        itoba(5, clientMessage);
        itoba(168, &clientMessage[4]);
    while (1) {
        int counter = 8;
        for (int i = 0; i < 8; ++i) {
            itoba(players[0].playerId, &clientMessage[counter]);
            counter = counter + 4;
            itoba(players[0].playerPositionX, &clientMessage[counter]);
            counter = counter + 4;
            itoba(players[0].playerPositionY, &clientMessage[counter]);
            counter = counter + 4;
            itoba(players[0].playerState, &clientMessage[counter]);
            counter = counter + 4;
            itoba(players[0].playerType, &clientMessage[counter]);
            counter = counter + 4;
        }
        
        int sent = write(socket, clientMessage, 168);
        if (sent < 0) {
            perror("Error sendPlayersLocationFunction");
            break;
        }
        sleep(1);
    }
    exit(1);
}

void *updatePlayersLocationFunction(void *arg) {
    while (1) {
        for (int i = 0; i < 8; ++i) {
            if (players[i].playerId == 0) {
                break;
            } else {
                int x = players[i].playerPositionX;
                int y = players[i].playerPositionY;
                switch(players[i].direction) {
                    //Top
                    case 0  :
                        if (map[x][y-1] != '2')
                            players[i].playerPositionY -= 1;
                        break;
                    //Bottom
                    case 1  :
                        if (map[x][y+1] != '2')
                            players[i].playerPositionY += 1;
                        break;
                    //Right
                    case 2  :
                        if (map[x-1][y] != '2')
                            players[i].playerPositionX -= 1;
                        break;
                    //Left
                    case 3  :
                        if (map[x+1][y] != '2')
                            players[i].playerPositionX += 1;
                        break;
                  
                    default : 
                        break;
                }
            }
        }
        sleep(1);
    }
}