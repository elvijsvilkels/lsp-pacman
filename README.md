# LSP Pacman game #
##Izveidoja:##
* Elvijs Ernests Viļķels - ev14027
* Raivis Ginters - rg14013
## LSP Tīkla spēle ##
### Kā kompilēt: ###
* `gcc server.c -o server -pthread`
* `gcc client.c -o client -pthread`

### Kā palaist programmas: ###
* `./server`
* `./client /serverIP/ 4200`