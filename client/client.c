#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <termios.h>
#include <fcntl.h>
#include <pthread.h> //threads


#define BUFFER_SIZE 10004 //izdomaaaas
#define STDIN_FILENO 0

// ======================== Function prototypes ================================================= //

int32_t batoi(const unsigned char bytes[4]); //byte array to int32_t conversion
void itoba(int32_t integer, unsigned char buffer[4]);  //int32_t to byte array conversion

void *handleListen(void *socket); //thread function listening to TCP messages
void createMap(unsigned char mapObject[][100],unsigned char *array);  //put map string values in mapObject 2d array
void drawMap(unsigned char mapObject[][100]);

int kbhit();    // to detect keyboard button press
int getch();    // get char from keyboard press
void move(int clientSocket, int32_t playerId, int32_t dir);
void quitGame(int clientSocket, int32_t playerId);


// ======================== Player struct array ================================================= //

typedef struct Player {
    int32_t playerId;
    unsigned char playerName[20];
    int32_t playerXCord;
    int32_t playerYCord;
    int32_t playerScore;
    int32_t playerState; //0-normal 1-dead 2-powerpellet 4-invincible 
    int32_t playerType; // 0-pacman 1-ghost
    } Player_t;

Player_t players[8];

// ======================== MAP values ================================================= //
    
    unsigned char mapObject[100][100];
    int32_t width;
    int32_t height;


int main(int argc, char *argv[]) {
	
	// define TCP comunication values here
	
	int serverPort;
	int conStatus;

	struct sockaddr_in serv_addr;
    struct hostent *server;

    //Thread id 
    pthread_t thread;

    // check if program was executed correctly
    if (argc < 3) {
        fprintf(stderr,"To execute program correctly: %s serverIPaddrs 4200\n", argv[0]);
        exit(1);
    }

    // get server IP address and server PORT 
    server = gethostbyname(argv[1]);

    serverPort = atoi(argv[2]);

    if (server == NULL) {
        fprintf(stderr,"Non existing host IP address!");
        exit(1);
    }

    // create client socket for connection
	int clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	// fcntl(clientSocket, F_SETFL, O_NONBLOCK);
	if (clientSocket < 0) {
		perror("Error opening socket!");
		exit(1);
	}

	// make a connection to a server
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(serverPort);

    conStatus = connect(clientSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    
    if (conStatus < 0) {
    	perror("Error while establishing connection to server!");
    	exit(1);
    }else {
    	printf("Connected!\n"); //Couse connected to server!
        printf("CERTIFIED AND CONFIRMED PACMAN GAME\n");
        //Send join packet
        unsigned char packet[24];
        int32_t packetType = 0;
        itoba(packetType, packet);

        printf("Join PACMAN game? \nYour username [20]: ");
        fgets(players[0].playerName,20,stdin);
        strncat(&packet[4], players[0].playerName, 20);


        printf("%s\n", &packet[4]);
        int send = write(clientSocket, packet, 24);

        if (send < 0) {
            perror("Error while sending packet!");
            exit(1);
        }

        //start listening to port!
        if (pthread_create(&thread, NULL, handleListen, &clientSocket) != 0) {
            perror("Failed to create readthread");
        }
    }
    
    for(;;) {
        // printf("~~~~~~MAIN~~~~~~\n");
        char control;
        int32_t dir;
        if(kbhit()){
        control = getch();
            switch (control){
                case 'w': 
                    // printf("ievadīji: w\n");  //debug purposes 
                    dir = 0;
                    move(clientSocket, players[0].playerId, dir);
                    break;
                case 'a':
                    // printf("ievadīji: a\n"); //debug purposes
                    dir = 2;
                    move(clientSocket, players[0].playerId, dir); 
                    break;
                case 's': 
                    // printf("ievadīji: s\n"); //debug purposes
                    dir = 1;
                    move(clientSocket, players[0].playerId, dir);
                    break;
                case 'd': 
                    // printf("ievadīji: d\n"); //debug purposes
                    dir = 3;
                    move(clientSocket, players[0].playerId, dir); 
                    break;
                case '+':
                    // printf("ievadīji: +\n"); //debug purposes
                    quitGame(clientSocket, players[0].playerId);
                    break;    
                default: break;
            }
        }
   }
	return 0;
}

// ======================== Functions ================================================= //

// thread function to handle TCP incoming messages
void *handleListen(void *socket) {

    unsigned char recMessage[BUFFER_SIZE]; 
    bzero(recMessage,BUFFER_SIZE);  

    int socketPtr = *((int*) socket);
    int clientSocket = socketPtr;
    
    while (1) {

        int recieve = read(clientSocket, recMessage, sizeof(recMessage));
        if (recieve < 0) {
            perror("Error while reading packet!");
            exit(1);
        }

        int32_t ptype = batoi(recMessage);

        
        if (ptype == 1) {
            printf("ACK\n");
            players[0].playerId = batoi(&recMessage[4]);
            printf("pID: %d\n", players[0].playerId);
        } 
        else if (ptype == 2) {
            printf("START GAME\n");

            width = batoi(&recMessage[4]);
            height = batoi(&recMessage[8]);
            players[0].playerXCord = batoi(&recMessage[12]);
            players[0].playerYCord = batoi(&recMessage[16]);

            printf("map: x: %d y: %d\n", height, width);
            printf("player: %s ID: %d COORDINATES x: %d y: %d\n", players[0].playerName, players[0].playerId, players[0].playerXCord, players[0].playerYCord);
        } 
        else if (ptype == 3) {
            printf("END GAME\n");
        } 
        else if (ptype == 4) {
            printf("MAP\n");
            // printf("rec: %s\n", &recMessage[4] );
            createMap(mapObject,&recMessage[4]);
            drawMap(mapObject);
            // char pl[1] = 's';
            
            
        }
        else if (ptype == 5) {
            // printf("PLAYERS\n");
            int32_t objCount = batoi(&recMessage[4]);

            int counter = 8;
            for (int i = 0; i < 8; ++i)
            {
               players[i].playerId = batoi(&recMessage[counter]);
               counter += 4;
               players[i].playerXCord = batoi(&recMessage[counter]);
               counter += 4;
               players[i].playerYCord = batoi(&recMessage[counter]);
               counter += 4;
               players[i].playerState = batoi(&recMessage[counter]);
               counter += 4;
               players[i].playerType = batoi(&recMessage[counter]);
               counter +=4;
            }

            // printf("Player print:\n");
            // for (int i = 0; i < 8; ++i) {
                //printf("player id: %d Pos: x %d y %d State: %d Type: %d\n", players[i].playerId, players[i].playerXCord, players[i].playerYCord, players[i].playerState, players[i].playerType);
            // }

        }
        else if (ptype == 6) {
            printf("SCORE\n");
        }
        else if (ptype == 10) {
            printf("JOINED\n");

        }

        //when server shuts down
        if (recieve == 0){
            printf("Server SHUT DOWN\n");
            exit(1);
        }

    }
}

//Convert 4 byte char array to int32_t
int32_t batoi(const unsigned char bytes[4]) {
    return (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
}

// Convert int32_t integers to 4 byte char array
void itoba(int32_t integer, unsigned char buffer[4]) {
    buffer[0] = (integer >> 24) & 0xFF;
    buffer[1] = (integer >> 16) & 0xFF;
    buffer[2] = (integer >> 8) & 0xFF;
    buffer[3] = integer & 0xFF;
}

void createMap(unsigned char mapObject[][100],unsigned char *array) {
    // zīmet karti funckija
    int counter = 0;
    
    for (int i=0; i < height;i++){
        for (int j = 0; j < width; j++)
        {
         mapObject[i][j] = array[counter];
         counter++;
         // printf("%d\n",counter);
        }
    }
}
void drawMap(unsigned char mapObject[][100]) {
    for (int i=0; i< height;i++) {
        for (int j = 0; j < width; j++) {
            char ch;
            // for (int z = 0; z < 9; ++z)
            // {
                if (i == players[0].playerYCord && j == players[0].playerXCord){
                // strcpy(&ch,"3");
                    printf("%c", ' ');
                    printf("%c",'P');
                    printf("%c", ' ');
                    // j++;
                    // break;
                    continue;
                }
            // }
            ch = mapObject[i][j];
            switch(ch) {
                case '0':
                    printf("%c", ' ');
                    printf("%c", ' ');
                    printf("%c", ' ');
                    break;
                case '1':
                    printf("%c", ' ');
                    printf("%c", '.');
                    printf("%c", ' ');
                    break;
                case '2':
                    if (i==0 && j==0){
                    printf("%c", ' ');
                    printf("%c", '_');
                    printf("%c", ' ');
                    } else if (i==0 && j==width-1){
                    printf("%c", ' ');
                    printf("%c", '_');
                    printf("%c", ' ');
                    } else if(i==height-1 && j==0){
                    printf("%c", ' ');
                    printf("%c", '\\');
                    printf("%c", ' ');
                    } else if (i==height-1 && j==width-1){
                    printf("%c", ' ');
                    printf("%c", '/');
                    printf("%c", ' '); 
                    } else if(i==0 || i==height-1){
                    printf("%c", ' ');
                    printf("%c", '_');
                    printf("%c", ' ');
                    }
                    else{
                    printf("%c", ' ');
                    printf("%c", '|');
                    printf("%c", ' '); 
                    }
                    break;
                default: break;    
            }

         // printf("%c",mapObject[i][j]);
        }
        printf("\n");
            }
}
int kbhit() {
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}

int getch() {
  return fgetc(stdin);
}

void move(int clientSocket, int32_t playerId, int32_t dir) {
    char packet[24];
    int32_t packetType = 7;
    itoba(packetType, packet);
    itoba(playerId, &packet[4]);
    itoba(dir, &packet[8]);
    int send = write(clientSocket, packet, 24);

        if (send < 0) {
            perror("Error while sending packet!");
            exit(1);
        }
}
void quitGame(int clientSocket, int32_t playerId) {

    char packet[8];
    int32_t packetType = 9;
    itoba(packetType, packet);
    itoba(playerId, &packet[4]);
    int send = write(clientSocket, packet, 8);

            if (send < 0) {
                perror("Error while sending packet!");
                exit(1);
            }
}